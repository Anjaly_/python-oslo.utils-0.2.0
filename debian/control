Source: python-oslo.utils
Section: python
Priority: optional
Maintainer: PKG OpenStack <openstack-devel@lists.alioth.debian.org>
Uploaders: Julien Danjou <acid@debian.org>,
           Thomas Goirand <zigo@debian.org>,
           Mehdi Abaakouk <sileht@sileht.net>,
           Gonéri Le Bouder <goneri@debian.org>
Build-Depends: debhelper (>= 9),
               openstack-pkg-tools,
               python-all (>= 2.6.6-3~),
               python-pbr,
               python-setuptools,
               python-sphinx,
               python3-all,
               python3-pbr,
               python3-setuptools
Build-Depends-Indep: python-babel (>= 1.3),
                     python-coverage (>= 3.6),
                     python-fixtures (>= 0.3.14),
                     python-hacking,
                     python-iso8601 (>= 0.1.9),
                     python-mock (>= 1.0),
                     python-oslo.config (>= 1.2.0),
                     python-oslo.i18n (>= 0.1.0),
                     python-oslosphinx,
                     python-oslotest,
                     python-six (>= 1.6.0),
                     python-subunit (>= 0.0.18),
                     python-testscenarios (>= 0.4),
                     python-testtools (>= 0.9.34),
                     python3-babel (>= 1.3),
                     python3-coverage (>= 3.6),
                     python3-fixtures (>= 0.3.14),
                     python3-iso8601 (>= 0.1.9),
                     python3-mock (>= 1.0),
                     python3-oslo.config (>= 1.2.0),
                     python3-oslo.i18n (>= 0.1.0),
                     python3-oslotest,
                     python3-six (>= 1.6.0),
                     python3-subunit (>= 0.0.18),
                     python3-testscenarios (>= 0.4),
                     python3-testtools (>= 0.9.34),
                     subunit (>= 0.0.18),
                     testrepository (>= 0.0.18)
Standards-Version: 3.9.5
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=openstack/python-oslo.utils.git
Vcs-Git: git://anonscm.debian.org/openstack/python-oslo.utils.git
Homepage: http://launchpad.net/oslo

Package: python-oslo.utils
Architecture: all
Pre-Depends: dpkg (>= 1.15.6~)
Depends: python-babel (>= 1.3),
         python-iso8601 (>= 0.1.9),
         python-oslo.config (>= 1.2.0),
         python-oslo.i18n (>= 0.1.0),
         python-six (>= 1.6.0),
         ${misc:Depends},
         ${python:Depends}
Description: set of utility functions for OpenStack - Python 2.x
 The Oslo.utils package provides a set of function which are cross-project
 for OpenStack. For example, it provides text decoding, exception handling,
 Python module import facility (try_import function), network-related
 system-level, and time utilities and helper functions.
 .
 This package contains the Python 2.x module.

Package: python3-oslo.utils
Architecture: all
Pre-Depends: dpkg (>= 1.15.6~)
Depends: python3-babel (>= 1.3),
         python3-iso8601 (>= 0.1.9),
         python3-oslo.config (>= 1.2.0),
         python3-oslo.i18n (>= 0.1.0),
         python3-six (>= 1.6.0),
         ${misc:Depends},
         ${python3:Depends}
Description: set of utility functions for OpenStack - Python 3.x
 The Oslo.utils package provides a set of function which are cross-project
 for OpenStack. For example, it provides text decoding, exception handling,
 Python module import facility (try_import function), network-related
 system-level, and time utilities and helper functions.
 .
 This package contains the Python 3.x module.

Package: python-oslo.utils-doc
Section: doc
Architecture: all
Pre-Depends: dpkg (>= 1.15.6~)
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Oslo Utility library - doc
 The Oslo.utils package provides a set of function which are cross-project
 for OpenStack. For example, it provides text decoding, exception handling,
 Python module import facility (try_import function), network-related
 system-level, and time utilities and helper functions.
 .
  This package contains the documentation.
